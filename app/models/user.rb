class User < ApplicationRecord
	store :settings, accessors: [ :host_video, :participants_video, :join_before_host ],  code: JSON
end
